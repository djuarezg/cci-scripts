#!/usr/bin/python
import logging
import logging.config
import json
import sys
import yaml

from argparse import ArgumentParser
from sqlalchemy import create_engine, Column, String, Integer, Text
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from yaml import Loader, SafeLoader

BASE = declarative_base()
LOGGER = logging.getLogger(__name__)
actions = {}
usages = {}


def construct_yaml_str(self, node):
    # Override the default string handling function
    # to always return unicode objects
    return self.construct_scalar(node)


Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)


def MediumText():
    return Text().with_variant(MEDIUMTEXT(), 'mysql')


class NovaBlockDeviceMapping(BASE):
    __tablename__ = "block_device_mapping"
    id = Column(Integer, primary_key=True, autoincrement=True)
    deleted = Column(Integer)
    connection_info = Column(MediumText())
    destination_type = Column(String(255))


class CinderVolume(BASE):
    __tablename__ = "volumes"
    id = Column(String(36), primary_key=True)
    deleted = Column(Integer)
    volume_type_id = Column(String(36))


class CinderVolumeAttachment(BASE):
    __tablename__ = "volume_attachment"
    id = Column(String(36), primary_key=True)
    volume_id = Column(String(36))
    connection_info = Column(Text)
    attach_status = Column(String(255))
    deleted = Column(Integer)


def _get_configuration(args):
    # Open the configuration file with the required information
    with open(args.config, 'r') as ymlFile:
        config = yaml.load(ymlFile, Loader=yaml.BaseLoader)
    return config


def _get_volume_types(config):
    # Fetch the information from Cinder about the types
    volume_to_types_map = {}
    volume_types_map = {}
    volume_types = config['cinder']['types']

    for volume_type in volume_types:
        name = config['cinder']['types'][volume_type]['name']
        volume_types_map[name] = volume_type

    # create an engine for Cinder
    LOGGER.debug('Connecting to Cinder Database')
    cinder_engine = create_engine(config['cinder']['database'])
    CinderSession = sessionmaker(bind=cinder_engine)
    cinder_session = CinderSession()

    LOGGER.debug('Retrieving volume types map')
    for volume in cinder_session.query(CinderVolume).\
            filter(CinderVolume.deleted == 0):
        if volume.volume_type_id in volume_types:
            volume_to_types_map[volume.id] = volume.volume_type_id

    LOGGER.debug('Closing the connection with Cinder Database')
    cinder_session.close()

    return {'type': volume_types,
            'name': volume_types_map,
            'volume': volume_to_types_map}


def _loop_and_fix_nova_dbs(args, config, volume_types):
    # apply filter by cell name if required
    nova_dbs = config['nova']['database']
    if args.cell and args.cell in nova_dbs:
        nova_dbs = {args.cell: nova_dbs[args.cell]}

    LOGGER.debug('Looping over nova databases')
    for nova_db in nova_dbs:
        # create an engine for Nova
        LOGGER.debug('[%s] Connecting to the database', nova_db)
        nova_engine = create_engine(config['nova']['database'][nova_db])
        NovaSession = sessionmaker(bind=nova_engine)
        nova_session = NovaSession()

        LOGGER.debug('[%s] Retrieving block_device_mapping entries', nova_db)
        bdms = nova_session.query(NovaBlockDeviceMapping).\
            filter(NovaBlockDeviceMapping.deleted == 0).\
            filter(NovaBlockDeviceMapping.destination_type == 'volume').all()

        iteration = 0
        total = len(bdms)
        LOGGER.debug('[%s] Found %s entries', nova_db, total)
        for bdm in bdms:
            changes = False
            iteration = iteration + 1
            LOGGER.debug('[%s] [%s/%s] Loading connection info...',
                         nova_db, iteration, total)
            try:
                connection = json.loads(bdm.connection_info)
            except Exception:
                LOGGER.info('[%s] [%s/%s] Error while loading the json data '
                            'skipping...', nova_db, iteration, total)
                continue

            if not connection or 'data' not in connection:
                LOGGER.info('[%s] [%s/%s] BDM has not valid connection data '
                            'skipping...', nova_db, iteration, total)
                continue

            if 'volume_id' not in connection['data']:
                volume_id = connection['serial']
            else:
                volume_id = connection['data']['volume_id']

            LOGGER.debug('[%s] [%s/%s] Processing bdm for %s',
                         nova_db, iteration, total, volume_id)
            if volume_id not in volume_types['volume']:
                LOGGER.info('[%s] [%s/%s] Volume id (%s) is not in the types '
                            'map skipping...', nova_db, iteration, total,
                            volume_id)
                continue
            LOGGER.debug('[%s] [%s/%s] Volume is in the list retrieving type',
                         nova_db, iteration, total)

            volume_type_id = volume_types['volume'][volume_id]

            if args.type and args.type in volume_types['name'] and \
                    volume_types['name'][args.type] != volume_type_id:
                LOGGER.info('[%s] [%s/%s] Volume type name does not'
                            ' match vol_type filter skipping...',
                            nova_db, iteration, total)
                continue

            if volume_type_id not in volume_types['type']:
                LOGGER.info('[%s] [%s/%s] Volume type not in the types map'
                            ' skipping...', nova_db, iteration, total)
                continue

            if args.fix_mon:
                changes = changes or _fix_key(
                    db=nova_db,
                    iteration=iteration,
                    total=total,
                    key='hosts',
                    is_json=True,
                    volume_id=volume_id,
                    connection_data=connection['data'],
                    volume_type=volume_types['type'][volume_type_id])
                changes = changes or _fix_key(
                    db=nova_db,
                    iteration=iteration,
                    total=total,
                    key='ports',
                    is_json=True,
                    volume_id=volume_id,
                    connection_data=connection['data'],
                    volume_type=volume_types['type'][volume_type_id])

            if args.fix_qos:
                changes = changes or _fix_key(
                    db=nova_db,
                    iteration=iteration,
                    total=total,
                    key='qos_specs',
                    is_json=False,
                    volume_id=volume_id,
                    connection_data=connection['data'],
                    volume_type=volume_types['type'][volume_type_id])

            if args.fix_cluster:
                changes = changes or _fix_key(
                    db=nova_db,
                    iteration=iteration,
                    total=total,
                    key='cluster_name',
                    is_json=False,
                    volume_id=volume_id,
                    connection_data=connection['data'],
                    volume_type=volume_types['type'][volume_type_id])

            LOGGER.warning('[%s] [%s/%s] Doing change on volume %s',
                           nova_db, iteration, total, volume_id)
            if not args.dryrun and \
                    (args.fix_mon or args.fix_qos or args.fix_cluster):
                if changes or args.force:
                    LOGGER.debug('[%s] [%s/%s] [%s] Dumping JSON data in bdm',
                                 nova_db, iteration, total, volume_id)
                    bdm.connection_info = json.dumps(connection,
                                                     ensure_ascii=False)
                    LOGGER.debug('[%s] [%s/%s] [%s] Committing change in db',
                                 nova_db, iteration, total, volume_id)
                    nova_session.commit()
                else:
                    LOGGER.info('[%s] [%s/%s] [%s] Value already present '
                                'skipping write to the db...', nova_db,
                                iteration, total, volume_id)
        nova_session.close()


def _loop_and_fix_cinder_db(args, config, volume_types):
    # create an engine for Cinder
    LOGGER.debug('Connecting to Cinder Database')
    cinder_engine = create_engine(config['cinder']['database'])
    CinderSession = sessionmaker(bind=cinder_engine)
    cinder_session = CinderSession()

    LOGGER.debug('Retrieving volume attachments')
    attachs = cinder_session.query(CinderVolumeAttachment).\
        filter(CinderVolumeAttachment.deleted == 0).\
        filter(CinderVolumeAttachment.attach_status == 'attached').\
        filter(CinderVolumeAttachment.connection_info.isnot(None)).all()

    iteration = 0
    total = len(attachs)
    LOGGER.debug('[cinder] Found %s entries', total)
    for attach in attachs:
        changes = False
        iteration = iteration + 1

        LOGGER.debug('[cinder] [%s/%s] Loading connection info...',
                     iteration, total)

        connection = json.loads(attach.connection_info)
        volume_id = attach.volume_id
        LOGGER.debug('[cinder] [%s/%s] Processing connection_info for %s',
                     iteration, total, volume_id)
        if volume_id not in volume_types['volume']:
            LOGGER.info('[cinder] [%s/%s] Volume id (%s) is not in the types '
                        'map skipping...', iteration, total, volume_id)
            continue

        LOGGER.debug('[cinder] [%s/%s] Volume is in the list retrieving type',
                     iteration, total)

        volume_type_id = volume_types['volume'][volume_id]

        if args.type and args.type in volume_types['name'] and \
                volume_types['name'][args.type] != volume_type_id:
            LOGGER.info('[cinder] [%s/%s] Volume type name does not'
                        ' match vol_type filter skipping...',
                        iteration, total)
            continue

        if volume_type_id not in volume_types['type']:
            LOGGER.info('[cinder] [%s/%s] Volume type not in the types map '
                        'skipping...', iteration, total)
            continue

        if args.fix_mon:
            changes = changes or _fix_key(
                db='cinder',
                iteration=iteration,
                total=total,
                key='hosts',
                is_json=True,
                volume_id=volume_id,
                connection_data=connection,
                volume_type=volume_types['type'][volume_type_id])
            changes = changes or _fix_key(
                db='cinder',
                iteration=iteration,
                total=total,
                key='ports',
                is_json=True,
                volume_id=volume_id,
                connection_data=connection,
                volume_type=volume_types['type'][volume_type_id])

        if args.fix_qos:
            changes = changes or _fix_key(
                db='cinder',
                iteration=iteration,
                total=total,
                key='qos_specs',
                is_json=False,
                volume_id=volume_id,
                connection_data=connection,
                volume_type=volume_types['type'][volume_type_id])

        if args.fix_cluster:
            changes = changes or _fix_key(
                db='cinder',
                iteration=iteration,
                total=total,
                key='cluster_name',
                is_json=False,
                volume_id=volume_id,
                connection_data=connection,
                volume_type=volume_types['type'][volume_type_id])

        LOGGER.warning('[cinder] [%s/%s] Doing change on volume %s',
                       iteration, total, volume_id)
        if not args.dryrun and \
                (args.fix_mon or args.fix_qos or args.fix_cluster):
            if changes or args.force:
                LOGGER.debug('[cinder] [%s/%s] [%s] Dumping JSON data in '
                             'connection_info', iteration, total, volume_id)
                attach.connection_info = json.dumps(connection,
                                                    ensure_ascii=False)
                LOGGER.debug('[cinder] [%s/%s] [%s] Committing change in db',
                             iteration, total, volume_id)
                cinder_session.commit()
            else:
                LOGGER.info('[cinder] [%s/%s] [%s] Value already present '
                            'skipping write to the db...', iteration, total,
                            volume_id)

    LOGGER.debug('Closing the connection with Cinder Database')
    cinder_session.close()


def _fix_key(db, iteration, total, key, is_json, volume_id, connection_data,
             volume_type):
    LOGGER.debug('[%s] [%s/%s] Fetching old/new %s information',
                 db, iteration, total, key)

    old_value = connection_data[key]
    str_old_value = json.dumps(old_value) if is_json else old_value
    LOGGER.info('[%s] [%s/%s] [%s] old_value=%s',
                db, iteration, total, volume_id, str_old_value)

    new_value = volume_type[key]
    str_new_value = json.dumps(new_value) if is_json else new_value
    LOGGER.info('[%s] [%s/%s] [%s] new_value=%s',
                db, iteration, total, volume_id, str_new_value)

    LOGGER.debug('[%s] [%s/%s] [%s] Applying %s change',
                 db, iteration, total, volume_id, key)
    connection_data[key] = new_value

    return old_value != new_value


def add_logging_arguments(parser):
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="increase output "
                        "to debug messages", action="store_true")


def configure_logging(args):
    level = logging.WARNING
    if args.verbose:
        level = logging.INFO
    elif args.debug:
        level = logging.DEBUG
    logging.basicConfig(level=level,
                        format="%(levelname)s: %(message)s")


def setup_argparse():
    parser = ArgumentParser(
        description='Fix hardcoded information in the block device mapping '
                    'in Nova and volume attachments in Cinder')

    parser.add_argument('--dryrun',
                        action='store_true',
                        help="Do not execute any actions")
    parser.add_argument('--force',
                        action='store_true',
                        help="Always overwrite the value stored in the db")
    parser.add_argument('--config',
                        required=True,
                        help="Location of configuration file")
    parser.add_argument('--cell',
                        default=None,
                        help='Apply changes ONLY on the cell specified')
    parser.add_argument('--type',
                        default=None,
                        help='Apply changes ONLY on the volume type specified')

    parser.add_argument(
        '--fix-mon',
        action='store_true',
        help='Fix the mon ips in nova/cinder databases.')

    parser.add_argument(
        '--fix-qos',
        action='store_true',
        help='Fix the qos_specs in nova/cinder databases.')

    parser.add_argument(
        '--fix-cluster',
        action='store_true',
        help='Fix the cluster_name in nova/cinder databases.')

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--nova",
        action='store_true',
        help="Apply fix only in Nova Databases")
    group.add_argument(
        "--cinder",
        action='store_true',
        help="Apply fix only in Cinder Database")
    group.add_argument(
        "--all",
        action='store_true',
        help="Apply fix on in All Databases")

    add_logging_arguments(parser)
    return parser


def main():
    parser = setup_argparse()
    args = parser.parse_args()
    configure_logging(args)

    config = _get_configuration(args)
    volume_types = _get_volume_types(config)
    if args.all or args.nova:
        _loop_and_fix_nova_dbs(args, config, volume_types)
    if args.all or args.cinder:
        _loop_and_fix_cinder_db(args, config, volume_types)


if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        LOGGER.exception(e)
        sys.exit(-1)
