﻿[string]$vmm 	= "cernvmm02.cern.ch"

[int]$labelSize = 16 	# for Format-CviTabRow
[int]$rowSize   = 12	# for Format-CviTabRow
[int]$rowCnt    = 4		# for Format-CviTabRow

$inputlist = @{	"Group 1" = "Patching OpenStack Servers - AZ 1";
				"Group 2" = "Patching OpenStack Servers - AZ 2";
				"Group 3" = "Patching OpenStack Servers - AZ 3"
			  }

$pssnapin_list = @()
$pssnapin_list += "sqlserverprovidersnapin100"
$pssnapin_list += "sqlservercmdletsnapin100"
#$pssnapin_list += "Microsoft.SystemCenter.VirtualMachineManager"


function Add-PSSnapindTosession($pssnapin_list) {
	$pssnapin_list | ForEach-Object {
		if ((Get-PSSnapin -Name $_ -ErrorAction SilentlyContinue) -eq $null ) {
			if ((Get-PSSnapin -Registered -Name $_ -ErrorAction SilentlyContinue) -ne $null) {
				Add-PSSnapin -Name $_ -WarningAction SilentlyContinue
			} else {
				Write-Host -Object ("# On " + $hostname + " host " + $_ + " PSSnapin is not available `n") -ForegroundColor yellow  
			}
		}
	}
}

#Remove-PSSnapin Microsoft.SystemCenter.VirtualMachineManager -ErrorAction SilentlyContinue
Remove-PSSnapin sqlserverprovidersnapin100 -ErrorAction SilentlyContinue
Remove-PSSnapin sqlservercmdletsnapin100 -ErrorAction SilentlyContinue

Add-PSSnapindTosession $pssnapin_list	

Import-Module ActiveDirectory -WarningAction SilentlyContinue

function Get-CmfNscExecutionTime([String]$NSCName) {
		
	invoke-sqlcmd -query ("SELECT PenRunDay + '/' + PenRunMonth + '/' + PenRunYear + ' ' + PenRunHour + ':' +PenRunMin AS TIME FROM NSC_List where Name = '" + $NSCName +"';") -database cmf -serverinstance cerncmfdb | %{ $_.TIME }
	
}

function Get-CmfNscMembers([String]$NSCName) {

	$NSCId = invoke-sqlcmd -query ("select ID from NSC_List where Name = '" + $NSCName +"';") -database cmf -serverinstance cerncmfdb | %{ $_.ID }

	Get-ADGroupMember ("CMF_NSC_" + $NSCId) | % { $_.name }

}		  


function Get-CviUpdateList {
<#
.Synopsis
 Gets a list of the hypervisors to update.
.Description
 Gets a list of the hypervisors to update.
.Parameter Count
 Number of all hypervisors.

.Example
 PS> Get-UpdateGroup
.Link
    about_functions
    about_functions_advanced
    about_functions_advanced_methods
    about_functions_advanced_parameters
.Notes
NAME:      Get-UpdateGroup
AUTHOR:    Sebastian Bukowiec, CERN IT-OIS
LASTEDIT:  06/06/2012
#>

	$hostname = & hostname

	# import necessary modules

#	Import-Module ActiveDirectory

	# import necessary pssnapins

	$pssnapin_list = @()
	$pssnapin_list += "sqlserverprovidersnapin100"
	$pssnapin_list += "sqlservercmdletsnapin100"


	function Add-PSSnapindTosession($pssnapin_list) {
		$pssnapin_list | ForEach-Object {
			if ((Get-PSSnapin -Name $_ -ErrorAction SilentlyContinue) -eq $null ) {
				if ((Get-PSSnapin -Registered -Name $_ -ErrorAction SilentlyContinue) -ne $null) {
					Add-PSSnapin -Name $_
				} else {
					Write-Host -Object ("# On " + $hostname + " host " + $_ + " PSSnapin is not available `n") -ForegroundColor yellow  
				}
			}
		}
	}

	Remove-PSSnapin sqlserverprovidersnapin100 -ErrorAction SilentlyContinue
	Remove-PSSnapin sqlservercmdletsnapin100 -ErrorAction SilentlyContinue
	Add-PSSnapindTosession $pssnapin_list		 

	function Get-CMFExecutionTime([String]$NSCName) {
		
		invoke-sqlcmd -query ("SELECT PenRunDay + '/' + PenRunMonth + '/' + PenRunYear + ' ' + PenRunHour + ':' +PenRunMin AS TIME FROM NSC_List where Name = '" + $NSCName +"';") -database cmf -serverinstance cerncmfdb | %{ $_.TIME }
	
	}

	$pc 		= Get-VMHost -VMMServer $vmm
	$cmfData 	= @{}
	
	# fill cmfData
	$inputlist.keys | %{ 
		$groupName = $_ 
		Get-CmfNscMembers $inputlist[$groupName] | %{ $cmfData.Add($_.ToLower(), $inputlist[$groupName]) }
	}  


	
		$inputlist.Values | Sort-Object | %{

			$cmfGrp 	= $_
			$tmpGrp 	= $cmfData.GetEnumerator() | where { $_.Value -eq $cmfGrp } | %{ $_.Name } 
			$cmfGrpCnt 	= $tmpGrp | measure | %{ $_.Count } 
			
			Write-Host -f Green $("`n" + $cmfGrp + "`n")
			
			#Import-Module S:\cvi\scripts\RDP.ps1
			
			$grpCnt = 0 
		
			$tmpGrp | % { $t = $_; $pc | where { $_.ComputerName -eq $t } } | select ComputerName, @{name="HostGroup";expression={$grp=$_.VMHostGroup.Path.TrimStart("All Hosts\").split("\");$grp[0]}} | group HostGroup | %{
			
				$grpName = $_.Name.Replace("Custom Hosts", "Self-Service")
				$grpData = $_.Group | %{ $_.ComputerName } | Sort-Object

				$grpCnt += $($grpData | measure | %{ $_.Count})
				
				Format-CviTabRow $grpName $grpData
				#Check-Rdp -Name $grpData
			}
			
			Write-Host -f DarkGray $("`n`t Hosts in the group".PadRight(20," ") + " : " + $grpCnt + " / CMF: " + $cmfGrpCnt)	
			Write-Host -f DarkGray $("`t Scheduled on ".PadRight(20," ") + " : " + $(Get-CMFExecutionTime $cmfGrp))
		}	
		Write-Host "`n"
}

function Get-CviUpdateDiff {

	$hostname = & hostname

	# import necessary modules

#	Import-Module ActiveDirectory

	# import necessary pssnapins

	$pssnapin_list = @()
	$pssnapin_list += "sqlserverprovidersnapin100"
	$pssnapin_list += "sqlservercmdletsnapin100"


	function Add-PSSnapindTosession($pssnapin_list) {
		$pssnapin_list | ForEach-Object {
			if ((Get-PSSnapin -Name $_ -ErrorAction SilentlyContinue) -eq $null ) {
				if ((Get-PSSnapin -Registered -Name $_ -ErrorAction SilentlyContinue) -ne $null) {
					Add-PSSnapin -Name $_
				} else {
					Write-Host -Object ("# On " + $hostname + " host " + $_ + " PSSnapin is not available `n") -ForegroundColor yellow  
				}
			}
		}
	}

	Remove-PSSnapin sqlserverprovidersnapin100 -ErrorAction SilentlyContinue
	Remove-PSSnapin sqlservercmdletsnapin100 -ErrorAction SilentlyContinue
	Add-PSSnapindTosession $pssnapin_list

	# script begin
		  
	#exlude list - todo

	$exlude = @()
	$exlude += "lxbrf37c04"

		 
	$pc = Get-VMHost -VMMServer $vmm
	
	$cmfData = @{}
	
	# fill cmfData
	$inputlist.keys | %{ 
		$groupName = $_ 
		Get-CmfNscMembers $inputlist[$groupName] | %{ $cmfData.Add($_.ToLower(), $inputlist[$groupName]) }
	}  
	
	## Stats 

	$vmmCnt 		= $pc | measure | %{ $_.Count }
	$cmfCnt 		= $cmfData.Count
	$grpCnt 		= @{}
	$vmmClrCnt 		= $pc | where { $_.HostCluster -ne $null } | measure | %{ $_.Count }
	$vmmNonClrCnt 	= $pc | where { $_.HostCluster -eq $null } | measure | %{ $_.Count }
	
	$inputlist.Values | %{  
		$grp = $_
		$tmpGrp = $cmfData.GetEnumerator() | where { $_.Value -eq $grp };
		$grpCnt.Add($_, $tmpGrp.Count)
	}
	Write-Host -f DarkGray $("`n############################################# Statistics ".PadRight(100,"#") + "`n");
	
	Write-Host -f Green $("No of hosts in VMM".PadRight(50, " ") + ": " + $vmmCnt);
	Write-Host -f DarkGreen $("`tClusterd (not considered)".PadRight(43, " ") + ": " + $vmmClrCnt);
	Write-Host -f DarkGreen $("`tNon clusterd (considered)".PadRight(43, " ") + ": " + $vmmNonClrCnt) -nonewline; Write-Host -f yellow $("   ( " + $($vmmNonClrCnt-$cmfCnt) + " )")
	Write-Host -f Green $("No of hosts in CMF".PadRight(50, " ") + ": " + $cmfCnt);
	$grpCnt.GetEnumerator() | %{
		Write-Host -f DarkGreen $("`t" + $_.Name.PadRight(42, " ") + ": " + $_.Value);
	}
	
	Write-Host -f DarkGray $("`n############################################# Diff ".PadRight(100,"#") + "`n");
	
	$pcList = @()

	#$pc | %{ $pcList += $_.ComputerName.ToLower() }
	
	$pc | %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $pcList += $name } }
	
	$cmfList = @()
	
	$cmfData.Keys | %{ $cmfList += $_ }
	
	Compare-Object $cmfList $pcList | % { Write-Host -f yellow $($_.InputObject + "`t" + (@{$true="add";$false="remove"}[$_.SideIndicator -eq "=>"]) + "`t" + $cmfData[$_.InputObject]) } 
	
# Additional Conditions
	
	Write-Host -f DarkGray $("`n############################################# Additional Conditions ".PadRight(100,"#") + "`n");
	
	# All IT-GT Etics Computers must to be in one group
	$etics = $pc| %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $name } } | where { $data -eq "Etics" }
	
	# compare all elements with the first one
	$etics | % { 
		if ($cmfData[$etics[0]] -ne $cmfData[$_]) { 
			Write-Host -f cyan $( "[Etics]".PadRight(14, " ")) -nonewline; 
			Write-Host -f red $_.PadRight(20, " ") -nonewline; 
			Write-Host -f white $( " not in one patch group : ") -nonewline; 
			Write-Host -f red $cmfData[$etics[0]] ;  
		}
	}
	
	$spares = $pc| %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $name } } | where { $data -eq "Spare machines" }

	$spares | % { 
		if ($inputlist["Spares"] -ne $cmfData[$_]) { 
			Write-Host -f cyan $( "[Spares]".PadRight(14, " ")) -nonewline; 
			Write-Host -f red $_.PadRight(20, " ") -nonewline; 
			Write-Host -f white $( " in : ") -nonewline; 
			Write-Host -f red $cmfData[$_] -nonewline;  
			Write-Host -f white " insted of: " -nonewline;
			Write-Host -f red $inputlist["Spares"]; 
		}
	}

	$critical = $pc| %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $name } } | where { 
		$data[0] -eq "IT-OIS" -and $data[1] -eq "Safehost" -or $data -eq "513 Critical Area" }

	$critical | % { 
		if ($inputlist["Critical Area"] -ne $cmfData[$_]) { 
			Write-Host -f cyan $( "[Critical]".PadRight(14, " ")) -nonewline; 
			Write-Host -f red $_.PadRight(20, " ") -nonewline; 
			Write-Host -f white $( " in : ") -nonewline; 
			Write-Host -f red $cmfData[$_] -nonewline;  
			Write-Host -f white " insted of: " -nonewline;
			Write-Host -f red $inputlist["Critical Area"];  
		}
	}
	
}

function Format-CviTabRow {

    param (
        [String]$Name,
		[Array]$Data
    )
    
    $preFormatedData = $Data | %{ (" " * ($rowSize-($_.Length))) + $_  }
        
    $cnt  = 0
    $iter = 0
    $dataCnt = $preFormatedData.Count
    
    Write-Host -f Cyan (" " * ($labelSize-$Name.Length) + $Name + "  :") -NoNewLine
    
    foreach ($i in $preFormatedData) {

        Write-Host $i -NoNewLine
        $cnt++
        $iter++
    
        if ($cnt -eq $rowCnt) {

            if (($dataCnt -gt $rowCnt) -and ($iter -lt $dataCnt)) {
                Write-Host "`n" -NoNewLine
                Write-Host (" " * ($labelSize+3)) -NoNewLine
            }
            $cnt = 0
        }
        
            
    }

    Write-Host "`n" -NoNewLine

}


function Send-OpenstackNotification {

	Param( 
		$VM,
		$Credential,
		$Template,
		$HostGroup,
		$Host,
		$ScheduleHost,
		[switch]$Send,
		[switch]$HTML
	)

	# LanDB Module is required
	Import-Module LanDB -WarningAction SilentlyContinue
	Import-Module Utils -WarningAction SilentlyContinue
	
	#$host 		= $null
	$cmfSchedule 	= @{}
	
		if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	# get data from CMF about the schedule / and use hash table insted asking DB x times
	#$inputlist.Values | %{ $cmfSchedule.Add($_, $(Get-CmfNscExecutionTime $_))  }
	
	#$pc = Get-VMHost -VMMServer "cernvmm02.cern.ch" #$vmm
	
	#if ($HostGroup -ne $null) {
		#$pc = Get-VMHost -VMMServer "cernvmm02.cern.ch" #$vmm / I move it level up, this line can be deleted

	#	$VM = $pc | %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $_.VMs | %{ $_.Name } } } | where { $data -eq $HostGroup }
		#$VM = $pc | %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $_.VMs | %{ $_.Name } } } | where { $data -eq "Custom Hosts" }
	#	Write-Host -f yellow $("Number of VMs in the " + $HostGroup + " : " + $($VM | measure | %{ $_.Count}) )
	#	if ($($VM | measure | %{ $_.Count }) -le 0) {
	#		Write-Host -f red $("No VMs in the " + $HostGroup + " #BREAK!#")
	#		break;
	#	}
	#}
	
	#if ($Host -ne $null) {
		#$pc = Get-VMHost -VMMServer "cernvmm02.cern.ch" #$vmm  / I move it level up, this line can be deleted
#		$VM = @()
		
	#	$Host | %{ $HstName = $_; $VM += $($pc | where { $_.ComputerName -eq $HstName } | % { $_.VMs } | %{ $_.Name }) }
		
		#$VM = $pc | where { $_.ComputerName -eq $Host } | % { $_.VMs } | %{ $_.Name }
		
	#	Write-Host -f yellow $("Number of VMs on the " + $Host + " : " + $($VM | measure | %{ $_.Count}) )
	#	if ($($VM | measure | %{ $_.Count }) -le 0) {
	#		Write-Host -f red $("No VMs on the " + $Host + " #BREAK!#")
	#		break;
	#	}
	#}
	
	# data from LanDB
	$data = Get-CviUserInfo -VM $VM -Credential $Credential
	
	$cmd 		= @()
	$cmfData 	= @{}
	
	# fill cmfData
	#$inputlist.keys | %{ 
	#	$groupName = $_ 
	#	Get-CmfNscMembers $inputlist[$groupName] | %{ $cmfData.Add($_.ToLower(), $inputlist[$groupName]) }
	#}  
	
	# hash table that contains e-mails that should be exclude from sending 
	# todo: add parametr that lets to add it during script run
	$outuser = @{}

    $data.Keys

	$data.Keys | %{ 
		$da = @{}; 
		$da.Add("#NAME#", $data[$_]["firstname"]); 
		$da.Add("#MAIL#", $data[$_]["email"]); 
		$da.Add("#VM#", $($data[$_]["vm"].Keys -join ', '));
		$($data[$_]["email"] + "," + $($data[$_]["vm"].Keys -join "`",`"" | %{ "@(`"" + $_ + "`")" })) | Out-File -FilePath $("./CVI_email_notif_" + $(Get-Date -uformat "%Y%m%d") + ".log") -Append  -Encoding utf8
		$vmList = "`r`n";
		$vmList += "`t" + "Virtual Machine".PadRight(24, " ") + "Intervention date`n"
		$vmList += "`t" + "=".PadRight(22, "=") + "  " + "=".PadRight(22, "=") + "`n"
		foreach ($vmName in $($data[$_]["vm"].Keys | Sort-Object)) {
			$VMHost 		= $data[$_]["vm"][$vmName]["host"]
	#		$VMHost_SCVMM 	= $($pc | %{ $_ } | %{ $_.VMs  } | where { $_.Name -eq $vmName } | %{ $_.HostName  }).split(".")[0]
			
			# compare data from LanDB and SCVMM
	#		if (!$($VMHost -eq $VMHost_SCVMM)) {
	#			Write-Host -f red $($vmName + " : WRONG HOST IN LanDB: " + $VMHost + " | Correct is: " + $VMHost_SCVMM)
				# use host from SCVMM
	#			$VMHost = $VMHost_SCVMM
	#		}
			
			# check if host is scheduled for any date
			if ($cmfData[$VMHost] -ne $null) {
				$schedule = $cmfSchedule[$cmfData[$VMHost]]
			} else {
				$schedule = ""
			}

			if (($ScheduleHost -ne $null) -and ($ScheduleHost.GetType().Name -eq "Hashtable")) {
				$vmList = $vmList + $("`t" + $vmName.PadRight(24, " ") +  $ScheduleHost[$VMHost]) + "`r`n"
			} else {
				$vmList = $vmList + $("`t" + $vmName.PadRight(24, " ") +  $schedule) + "`r`n"
			}

		}
		$da.Add("#VMLIST#", $vmList); 
		$vmCnt = $data[$_]["vm"].Keys | measure | %{ $_.Count }
		$da.Add("#VMCNT#", $vmCnt);
		$cmd += "Send-Mail";
		$cmd += "-To `$da[`"#MAIL#`"]";
		$cmd += "-Template `$Template";
		$cmd += "-Replace `$da"; 
		if ($Send -and !$outuser.Contains($data[$_]["email"])) { $cmd += "-Send" } else { Write-Host -f red $($data[$_]["email"] + " excluded") } ;
		if ($HTML) {$cmd += "-HTML"};
		Invoke-Expression $($cmd -join ' ');
		if (!$Send) { break; } # show only first e-mail from the list if the Send flag is not set
		$cmd = @()
	}

	Write-Host -f yellow $("E-mails sent: " + $($data.Keys | measure | %{ $_.Count }))
	
}

function Check-CVIHostInLanDB {

	Param( 
		$VM,
		$Credential,
		$HostGroup,
		$Host
	)

	# LanDB Module is required
	Import-Module LanDB -WarningAction SilentlyContinue
	Import-Module Utils -WarningAction SilentlyContinue
	
	#$host 		= $null
	$cmfSchedule 	= @{}
	
		if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	# get data from CMF about the schedule / and use hash table insted asking DB x times
	$inputlist.Values | %{ $cmfSchedule.Add($_, $(Get-CmfNscExecutionTime $_))  }
	
	if ($HostGroup -ne $null) {
		$pc = Get-VMHost -VMMServer "cernvmm02.cern.ch" #$vmm

		$VM = $pc | %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $_.VMs | %{ $_.Name } } } | where { $data -eq $HostGroup }
		#$VM = $pc | %{ if ($_.HostCluster -eq $null) { $name = $_.ComputerName.ToLower(); $data = $_.VMHostGroup.Path.TrimStart("All Hosts\").split("\"); $_.VMs | %{ $_.Name } } } | where { $data -eq "Custom Hosts" }
		Write-Host -f yellow $("Number of VMs in the " + $HostGroup + " : " + $($VM | measure | %{ $_.Count}) )
		if ($($VM | measure | %{ $_.Count }) -le 0) {
			Write-Host -f red $("No VMs in the " + $HostGroup + " #BREAK!#")
			break;
		}
	}
	
	if ($Host -ne $null) {
		$pc = Get-VMHost -VMMServer "cernvmm02.cern.ch" #$vmm

		$VM = $pc | where { $_.ComputerName -eq $Host } | % { $_.VMs } | %{ $_.Name }
		
		Write-Host -f yellow $("Number of VMs on the " + $Host + " : " + $($VM | measure | %{ $_.Count}) )
		if ($($VM | measure | %{ $_.Count }) -le 0) {
			Write-Host -f red $("No VMs on the " + $Host + " #BREAK!#")
			break;
		}
	}
	
	# data from LanDB
	$data = Get-CviUserInfo -VM $VM -Credential $Credential
	
	$cmd 		= @()
	
	# hash table that contains e-mails that should be exclude from sending 
	# todo: add parametr that lets to add it during script run
	$outuser = @{}

	$wrongLocation = @{}
	
	$data.Keys | %{ 
		$da = @{}; 
		$da.Add("#NAME#", $data[$_]["firstname"]); 
		$da.Add("#MAIL#", $data[$_]["email"]); 
		$da.Add("#VM#", $($data[$_]["vm"].Keys -join ', '));
		$vmList = "`r`n";
		$vmList += "`t" + "Virtual Machine".PadRight(24, " ") + "Hypervisor".PadRight(24, " ") + "Intervention date`n"
		$vmList += "`t" + "=".PadRight(22, "=") + "  " + "=".PadRight(22, "=") + "  " + "=".PadRight(22, "=") + "`n"
		foreach ($vmName in $($data[$_]["vm"].Keys) ) {
			$VMHost 		= $data[$_]["vm"][$vmName]["host"]
			$VMHost_SCVMM 	= $($pc | %{ $_ } | %{ $_.VMs  } | where { $_.Name -eq $vmName } | %{ $_.HostName  }).split(".")[0]
			
			# compare data from LanDB and SCVMM
			if (!$($VMHost -eq $VMHost_SCVMM)) {
				if (!$wrongLocation.Contains($vmName)) {
					$record = @{}
					$record.Add("LanDB", $VMHost)
					$record.Add("SCVMM", $VMHost_SCVMM)
					
					$wrongLocation.Add($vmName, $record)
					Write-Host -f red $($vmName.PadRight(30," ") + " : WRONG HOST IN LanDB: " + $wrongLocation[$vmName].Values)
				}
				
			} else {
				Write-Host -f green $($vmName.PadRight(30," ") + " : OK! : " + $VMHost)
			}
		}
				
		$cmd = @()
	}
	
	# display report
	Write-Host -f DarkGray $("`n`n============================ CHECK REPORT ============================`n")		
	if ($($wrongLocation.Keys | measure | %{ $_.Count}) -le 0) {
		Write-Host -f green "Everything is OK!"
	} else {
		$wrongLocation.Keys | Sort-Object | %{ Write-Host -f red $($_.PadRight(30," ") + " : " + $wrongLocation[$_].Values) }
	}
	Write-Host -f DarkGray $("`n========================== END CHECK REPORT ==========================")	
}

function Check-CviRdpConnnections {

	param(
		$Group
	)

	Import-Module S:\cvi\scripts\RDP.ps1
	
	$cmfGroups = @{}
	
	$i = 0
	$inputlist.Keys | %{
		$cmfGroups.Add($i, $inputlist[$_])
		$i++
	}
	
	if ($group -eq $null) {
	
		Write-Host -f yellow $("Choose a group to check: eg. Check-Rdp -Group 2 ")
		$cmfGroups 
	
	} else {
		$srvList = Get-CmfNscMembers $cmfGroups[$Group] 
		Check-Rdp $srvList
	}


}
