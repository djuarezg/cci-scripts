﻿function Get-LanDB {

	param(
		$Credential
	)
	
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }    
    $uriSOAP = "https://network.cern.ch/sc/soap/soap.fcgi?v=4&WSDL"
    $ws = New-WebServiceProxy -uri $uriSOAP 

    $wsTypes = @{}

    foreach ($Type in $ws.GetType().Assembly.GetExportedTypes()) 
    {
		$wsTypes.Add($Type.Name, $Type.FullName);
    }

    $ws.AuthValue = New-Object $wsTypes.Auth
    $ws.AuthValue.token = $ws.getAuthToken($credential.UserName, [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(($credential.Password))), "NICE")
		
    $ws
}

function Get-CviOwner {

	Param( 
		$VM,
		$Credential
	)
	
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	$ws = Get-LanDB -Credential $Credential
	
	$VM | % {
		$device = $ws.getDeviceInfo($_)
		$responsible = $device.ResponsiblePerson
		
		$_ + "`t`t`t" + $responsible.Email
	}

}

function Get-CviMainUser {

	Param( 
		$VM,
		$Credential
	)
	
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	$ws = Get-LanDB -Credential $Credential
	
	$VM | % {
		$device = $ws.getDeviceInfo($_)
		$user = $device.UserPerson
		
		$_ + "`t`t`t" + $user.Email
	}

}

function Get-CviUserInfo {

	Param( 
		$VM,
		$Credential
	)
	
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	$ws = Get-LanDB -Credential $Credential
	
	$output = @{}

	$VM | % {
		$device 		= $ws.getDeviceInfo($_)
		$hv				= $ws.vmGetInfo($_).VMParent
		$responsible 	= $device.ResponsiblePerson
		$mainUser		= $device.UserPerson	
		
		# check if the responsible user is already on the list
		if ($output.Contains($($responsible.Email))) {		
			# if yes - add vm to his list
			$svm	= @{}
			
			$svm.add("name", $device.DeviceName)
			$svm.add("desc", $device.Description)
			$svm.add("mac", $($device.NetworkInterfaceCards | %{ $_.HardwareAddress }))
			$svm.add("status", @("owner"))
			$svm.add("host", $hv)
			
			$output[$($responsible.Email)].vm.add($device.DeviceName, $svm)
		} else {
			# if not - create whole structure 
			$svm	= @{}
			$user	= @{}
			$vmList	= @{}
			
			$svm.add("name", $device.DeviceName)
			$svm.add("desc", $device.Description)
			$svm.add("mac", $($device.NetworkInterfaceCards | %{ $_.HardwareAddress }))
			$svm.add("status", @("owner"))
			$svm.add("host", $hv)
			
			$vmList.add($device.DeviceName, $svm)
			
			$user.add("firstname", $responsible.FirstName)
			$user.add("lastname", $responsible.Name)
			$user.add("email",$responsible.Email)
			$user.add("ccid",$responsible.CCID)
			$user.add("vm", $vmList)
			
			$output.add($($responsible.Email).ToLower(), $user)
		}
		# check if the main user is on the list
		if ($output.Contains($($mainUser.Email))) {		
			# check if the user has already the vm on the list
			if ($output[$($mainUser.Email)].vm.contains($device.DeviceName)) {
				# if yes - add only a new status
				$output[$($mainUser.Email)].vm[$device.DeviceName].status += "main"
			} else {
				#if not - add vm to the list of VMs of the user
				$svm	= @{}
				
				$svm.add("name", $device.DeviceName)
				$svm.add("desc", $device.Description)
				$svm.add("mac", $($device.NetworkInterfaceCards | %{ $_.HardwareAddress }))
				$svm.add("status", @("main"))
				$svm.add("host", $hv)
				
				$output[$($mainUser.Email).ToLower()].vm.add($device.DeviceName, $svm)
			}
		} else {
		# if is not on the list - create a whole structure
			$svm	= @{}
			$user	= @{}
			$vmList	= @{}
			
			$svm.add("name", $device.DeviceName)
			$svm.add("desc", $device.Description)
			$svm.add("mac", $($device.NetworkInterfaceCards | %{ $_.HardwareAddress }))
			$svm.add("status", @("main"))
			$svm.add("host", $hv)
			
			$vmList.add($device.DeviceName, $svm)
			
			$user.add("firstname", $mainUser.FirstName)
			$user.add("lastname", $mainUser.Name)
			$user.add("email",$mainUser.Email)
			$user.add("ccid",$mainUser.CCID)
			$user.add("vm", $vmList)
			
			$output.add($($mainUser.Email).ToLower(), $user)
		}
	}
	$output
}

function Set-CviVmName {

	Param( 
		$VM,
		$Name,
		$Credential
	)
	
	# get credential if they were not passed as parameter
	if ($Credential -eq $null) {
		$Credential = Get-Credential
    }   
	
	# gen landb soap obj
	$ws = Get-LanDB -Credential $Credential
	
	# declaration of a var that stores all types of the landb soap
	$wsTypes = @{}
	
	# fill $wsTypes var with types from landb obj
    foreach ($Type in $ws.GetType().Assembly.GetExportedTypes()) 
    {
		$wsTypes.Add($Type.Name, $Type.FullName);
    }
	
	# get Auth obj
    $ws.AuthValue = New-Object $wsTypes.Auth
    $ws.AuthValue.token = $ws.getAuthToken($Credential.UserName, [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR(($Credential.Password))), "NICE")
	
	# get device info of the VM that will have changed the name
	$sourceDevice = $ws.getDeviceInfo($VM)
	
	# declare a DeviceInput object
	$inputDevice =  New-Object $wsTypes.DeviceInput
	
	# declare a PersonInput object
	$responsiblePerson = New-Object $wsTypes.PersonInput
	
	$responsiblePerson.Name				= $sourceDevice.ResponsiblePerson.Name
	$responsiblePerson.FirstName		= $sourceDevice.ResponsiblePerson.FirstName
	$responsiblePerson.Department		= $sourceDevice.ResponsiblePerson.Department
	$responsiblePerson.Group			= $sourceDevice.ResponsiblePerson.Group
	
	# declare a UserPerson object	
	$userPerson = New-Object $wsTypes.PersonInput
	
	$userPerson.Name					= $sourceDevice.UserPerson.Name
	$userPerson.FirstName				= $sourceDevice.UserPerson.FirstName
	$userPerson.Department				= $sourceDevice.UserPerson.Department
	$userPerson.Group					= $sourceDevice.UserPerson.Group
	
	# fill the device input object with a new name and rest of not changed values from the source
	$inputDevice.DeviceName				= $Name.ToUpper()
	$inputDevice.Location				= $sourceDevice.Location
	$inputDevice.Zone					= $sourceDevice.Zone
	$inputDevice.Manufacturer			= $sourceDevice.Manufacturer
	$inputDevice.Model					= $sourceDevice.Model
	$inputDevice.Description			= $sourceDevice.Description
	$inputDevice.Tag					= $sourceDevice.Tag
	$inputDevice.SerialNumber			= $sourceDevice.SerialNumber
	$inputDevice.OperatingSystem		= $sourceDevice.OperatingSystem
	$inputDevice.InventoryNumber		= $sourceDevice.InventoryNumber
	$inputDevice.ResponsiblePerson		= $responsiblePerson
	$inputDevice.UserPerson				= $userPerson
	$inputDevice.HCPResponse			= $sourceDevice.HCPResponse

	$inputDevice
	
	# update landb 
	$ws.deviceUpdate($VM, $inputDevice)
}

