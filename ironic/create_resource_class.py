import os
import urllib3
import socket
from landbclient.client import LanDB

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

landb_username = "vmmaster"
landb_password = os.environ["LANDB_PASSWORD"]
landb_hostname = "network.cern.ch"
landb_port = "443"
landb_protocol = "https"

landb = LanDB(landb_username, landb_password,
              landb_hostname, landb_port, version=6)

nodes = [
    'dl7336891-c2130lg44n80017', 'dl7336891-c2130lg44n80018',
    'dl7336891-c2130lg44n80019', 'dl7336891-c2130lg44n80020',
    'dl7336891-c2130lg44n80021', 'dl7336891-c2130lg44n80076',
    'dl7336891-c2130lg44n80102', 'dl7336891-c2130lg44n80103',
    'dl7336891-c2130lg44n80104', 'dl7336891-c2130lg44n80106',
    'dl7336891-c2130lg49n80001', 'dl7336891-c2130lg49n80002',
    'dl7336891-c2130lg49n80008', 'dl7336891-c2130lg49n80009',
    'dl7336891-c2130lg49n80010', 'dl7336891-c2130lg49n80011',
    'dl7336891-c2130lg49n80032', 'dl7336891-c2130lg49n80034',
    'dl7336891-c2130lg49n80055',
]

for node in nodes:
    delivery = node[0: 9]
    node_ip = socket.gethostbyname(node)
    device = landb.device_info(landb.device_search(ip_addr=node_ip)[0])
    svc_name = device.Interfaces[0].ServiceName.replace('-', '_')
    resource_class = 'BAREMETAL_P1_{}_{}'.format(delivery.upper(), svc_name)
    ironic_command = "openstack baremetal node set {} --resource-class {}"
    print(ironic_command.format(node, resource_class))
