#!/usr/bin/env python

from __future__ import print_function

import argparse
import logging
import sys
import ConfigParser

import ast

import netaddr as netaddr
from sqlalchemy import MetaData
from sqlalchemy import select
from sqlalchemy import Table
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from aitools import foreman
from aitools.errors import AiToolsForemanError
import requests_kerberos

from subprocess import Popen, PIPE


class BMCAddressNotAvailable(Exception):
    pass


def makeConnection(db_url):
    engine = create_engine(db_url)
    engine.connect()
    Session = sessionmaker(bind=engine)
    thisSession = Session()
    metadata = MetaData()
    metadata.bind = engine
    Base = declarative_base()
    tpl = thisSession, metadata, Base

    return tpl


def parse_cmdline_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config",
                        default='ironic.conf',
                        help='configuration file')
    parser.add_argument("--debug",
                        action='store_true',
                        help='configuration file')
    parser.add_argument("--dryrun",
                        action='store_true',
                        help='read-only mode')
    return parser.parse_args()


def get_db_url(config_file, group, param):
    parser = ConfigParser.SafeConfigParser()
    try:
        parser.read(config_file)
        db_url = parser.get(group, param)
    except ConfigParser.Error as e:
        logging.error("ERROR: Check configuration file. %s" % str(e))
        sys.exit(2)
    return db_url


def get_baremetal_instances(ironic_meta, nova_meta):
    """Get deployed physical nodes and their corresponding nova instances.

    This method will iterate through all the ironic nodes with an instance
    assigned, ask nova about the hostname based on the instance uuid and
    return a list of all the instance names.
    """

    res = []

    try:
        nodes_table = Table('nodes', ironic_meta, autoload=True)
        instances_table = Table('instances', nova_meta, autoload=True)
    except Exception as e:
        logging.error("Something went wrong with loading DB schema: %s" % str(e))
        sys.exit(2)

    try:
        nodes = select(columns=[nodes_table.c.instance_uuid]).where(nodes_table.c.instance_uuid.isnot(None)).execute()
        # nodes = select(columns=[nodes_table.c.instance_uuid, nodes_table.c.driver_info, nodes_table.c.id, nodes_table.c.name]).where(nodes_table.c.instance_uuid.isnot(None)).execute()
    except Exception as e:
        logging.error("Cannot select ironic nodes: %s" % str(e))
        sys.exit(2)

    for node in nodes:
        try:
            logging.debug("Collecting data for instance '%s'..." % node[0])
            instance = get_instance_from_ironic_node(instances_table, node[0])
            # serial = node[3]

            # username, password, ipmi_ip = get_ipmi_creds_from_ironic_node(node[1])
            # mac = get_mac_address_from_ipmi_creds(ipmi_ip, username, password)

            # host_fqdn, ipmi_fqdn, mac = instance + ".cern.ch", serial + "-ipmi.cern.ch", mac

            # res.append((host_fqdn, ipmi_fqdn, mac, username, password, ipmi_ip))
            res.append(instance + ".cern.ch")
            # logging.debug("Collected data for instance '%s': '%s', '%s', '***', '%s'" % (node[0], mac, username, ipmi_ip))
            logging.debug("Collected data for instance '%s': '%s'" % (node[0], instance))
        except Exception as e:
            logging.error("Cannot collect informations about the instance: %s" % str(e))

    return res


def get_instance_from_ironic_node(instances_table, uuid):
    """Get nova instance name from uuid.

    Given nova instance uuid, return its hostname.
    """

    return select(columns=[instances_table.c.hostname]).where(uuid == instances_table.c.uuid).execute().fetchone()[0]


def get_ipmi_creds_from_ironic_node(driver_info):
    """Get IPMI creds from internal driver info

    Given internal ironic representation of driver_info field from nodes table,
    unpack IPMI username, password and IP address.
    """

    driver_info = ast.literal_eval(driver_info)
    username, password, ipmi_ip = driver_info["ipmi_username"], driver_info["ipmi_password"], driver_info["ipmi_address"]
    return username, password, ipmi_ip


def get_mac_address_from_ipmi_creds(ipmi_ip, username, password):
    try:
        # From all the channels 0-15, only 1-7 can be assigned to different
        # types of communication media and protocols and effectively used
        for channel in range(1, 8):
            out, e = Popen("ipmitool -H {} -U '{}' -P '{}' lan print {} | awk '/IP Address[[:space:]]*:/ {{print $4}}'".format(ipmi_ip, username, password, channel), shell=True, stdout=PIPE, stderr=PIPE).communicate()
            if e.startswith("Invalid channel"):
                continue
            out = out.strip()

            try:
                netaddr.IPAddress(out)
            except netaddr.AddrFormatError:
                logging.warning('Invalid IP address: %s', out)
                continue

            # In case we get 0.0.0.0 on a valid channel, we need to keep
            # querying
            if out != '0.0.0.0':
                out, e = Popen("ipmitool -H {} -U '{}' -P '{}' lan print {} | awk '/MAC Address[[:space:]]*:/ {{print $4}}'".format(ipmi_ip, username, password, channel), shell=True, stdout=PIPE, stderr=PIPE).communicate()
                return out.strip()

    except Exception as e:
        logging.error("Cannot get BMC MAC for '%s': %s" % (ipmi_ip, str(e)))
        raise BMCAddressNotAvailable

    # If the previous did not return any correct MAC address, we want to log
    # and raise an exception

    logging.error("Cannot get any BMC MAC for '%s'" % ipmi_ip)
    raise BMCAddressNotAvailable


def get_ironic_node_from_nova_instance(instances_table, hostname):
    """Get ironic node from nova instance name

    Given nova instance name, find uuid of the ironic node hosting it.
    """

    pass


def manage_foreman_entry(ForemanClient, dryrun, host_fqdn):
    """Make Foreman entry managed.

    This method will change the status of the host to "managed" in Foreman. In case host is already "managed",
    it won't be touched.

    Running this method will override the following set of parameters - (medium, ptable, os, architecture) to the
    predefined ones.
    """

    try:
        logging.debug("Checking instance '%s' in Foreman..." % host_fqdn)
        host = ForemanClient.gethost(host_fqdn)
        if dryrun:
            logging.info("Foreman access to instance '%s' confirmed" % host_fqdn)
            return
    except AiToolsForemanError:
        logging.warning("Cannot get instance '%s' from Foreman" % host_fqdn)
        return

    if not host["managed"]:
        logging.info("Updating instance '%s' in Foreman..." % host_fqdn)

        try:
            # medium = host["medium_name"] if host["medium_name"] else "CentOS mirror"$
            # ptable = host["ptable_name"] if host["ptable_name"] else "Kickstart default"
            # operatingsystem = host["operatingsystem_name"] if host["operatingsystem_name"] else "CentOS 7.4"
            # architecture = host["architecture_name"] if host["architecture_name"] else "x86_64"

            medium = "CentOS mirror"
            ptable = "Kickstart default"
            operatingsystem = "CentOS 7.4"
            architecture = "x86_64"

            ForemanClient.updatehost(host,
                                     medium=medium,
                                     ptable=ptable,
                                     managed=True,
                                     operatingsystem=operatingsystem,
                                     architecture=architecture
                                     )
            logging.info("Instance '%s' set to 'managed' in Foreman..." % host_fqdn)
        except requests_kerberos.exceptions.MutualAuthenticationError as e:
            # requests_kerberos.exceptions.MutualAuthenticationError: Unable to authenticate <Response [200]>
            logging.warning("Retrying to manage instance '%s' in Foreman: %s" % (host_fqdn, str(e)))
            ForemanClient.updatehost(host,
                                     medium=medium,
                                     ptable=ptable,
                                     managed=True,
                                     operatingsystem=operatingsystem,
                                     architecture=architecture
                                     )
            logging.info("Instance '%s' set to 'managed' in Foreman..." % host_fqdn)
        except AiToolsForemanError as e:
            logging.error("Cannot manage instance '%s' in Foreman: %s" % (host_fqdn, str(e)))
            return

        # try:
        #     ForemanClient.add_ipmi_interface(host_fqdn, ipmi_fqdn, mac, username, password, ipmi_ip)
        # except AiToolsForemanError as e:
        #     ForemanClient.updatehost(host)  # Revert "managed" status to the previous one
        #     logging.error("Cannot insert IPMI interface for instance '%s' in Foreman: %s" % (host_fqdn, str(e)))
        #     logging.error("Instance '%s' reverted to 'unmanaged' in Foreman..." % host_fqdn)
        #     return
        # else:
        #     logging.info("IPMI interface inserted for instance '%s' in Foreman..." % host_fqdn)
    else:
        logging.info("Instance '%s' already managed in Foreman..." % host_fqdn)


def main():
    try:
        args = parse_cmdline_args()
        if args.debug:
            logging.getLogger().setLevel(logging.DEBUG)
        else:
            logging.getLogger().setLevel(logging.INFO)
        if args.dryrun:
            logging.info("--- THIS IS A DRYRUN. NO CHANGES WILL BE MADE ---")
    except Exception as e:
        logging.error("Wrong command line arguments (%s)" % str(e))
        sys.exit(2)

    try:
        logging.debug("Connecting to the nova DB...")
        bm_db_url = get_db_url(args.config, 'novadb', 'bm_db')
        nova_session, nova_metadata, nova_Base = makeConnection(bm_db_url)

        logging.debug("Connecting to the ironic DB...")
        ironic_db_url = get_db_url(args.config, 'ironicdb', 'ironic_db')
        ironic_session, ironic_metadata, ironic_Base = makeConnection(ironic_db_url)

    except Exception as e:
        logging.error("Something went wrong with DB connection: %s" % str(e))
        sys.exit(2)

    machs = get_baremetal_instances(ironic_metadata, nova_metadata)

    try:
        ForemanClient = foreman.ForemanClient("judy.cern.ch", 8443, 60, deref_alias=True)
    except Exception as e:
        logging.error("Cannot initialize Foreman client: %s" % str(e))
        sys.exit(2)

    for mach in machs:
        manage_foreman_entry(ForemanClient, args.dryrun, mach)


if __name__ == "__main__":
    main()
