#!/bin/bash

OK=0
KO=0
OK_HOSTS=""
KO_HOSTS=""

for HOST in $RD_OPTION_HOSTS
  do
    echo "[INFO] Checking exceptions on host $HOST..."
    ACTIVE_EXCEPTION=$(ssh -q -o StrictHostKeyChecking=no root@$HOST lemon-host-check --script)
    if [[ $? -eq 0 ]]; then
      if [[ $ACTIVE_EXCEPTION == "" ]]; then
        echo "[INFO] lemon-host-check did not return any exception"
        OK_HOSTS="$OK_HOSTS $HOST"
        echo ""
        ((OK++))
      else
        ACTIVE_EXCEPTION=$(echo $ACTIVE_EXCEPTION|tr -d '\n')
        echo "[ERROR] lemon-host-check returned '$ACTIVE_EXCEPTION' exception."
        KO_HOSTS="$KO_HOSTS $HOST"
        echo ""
        ((KO++))
      fi
    else
      echo "[ERROR] Could not connect to '$HOST'. Please verify hostname and credentials"
      KO_HOSTS="$KO_HOSTS $HOST"
      echo ""
      ((KO++))
    fi
  done

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n SUMMARY LEMON-HOST-CHECK:     $((OK + KO)):   $OK:  $KO" | column  -t -s ':'
echo ""

if [ $KO -ne 0 ]; then
  echo "[ERROR] These hosts returned errors: $KO_HOSTS. Please fix them before running this job again."
  if [ ! -z "$OK_HOSTS" ]; then
    echo "[INFO] You can rerun this job only using the ones that did not return any errors, to do it please change the input host list to:"
    echo "$OK_HOSTS"
  fi
  if [ ${RD_OPTION_EXECUTION_MODE} == 'force' ]; then
    echo "[INFO] execution_mode set to 'force', continuing anyway..."
  else
    exit 1
  fi
fi
