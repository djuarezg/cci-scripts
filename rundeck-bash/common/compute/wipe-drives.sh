#!/bin/bash

echo "[INFO] Exporting default v3kerberos variables..."
export OS_PROJECT_NAME=admin
export OS_PROJECT_DOMAIN_ID=default
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_TYPE=v3fedkerb
export OS_IDENTITY_PROVIDER=sssd
export OS_PROTOCOL=kerberos
export OS_AUTH_URL=https://keystone.cern.ch/v3

if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]
then
    echo "INFO:rundeck: Executing ai-installhost to wipe disk during next startup on: $RD_OPTION_HOSTS..."
    ai-installhost -t 4 --aims-kopts "load_ramdisk=1 root=/dev/ram rw console=ttyS0,9600 console=ttyS1,9600 console=tty0 runscript=deathdisk_bash.sh runarg=keep-creds" --aims-target "U-CC-7.6-2-X86_64" ${RD_OPTION_HOSTS}
else
    echo "[DRYRUN] Would have wiped $RD_OPTION_HOSTS disks"
fi

echo ""
